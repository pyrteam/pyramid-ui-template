import {ValidationResult} from '@app/utils/form/validationResult';

export class FormFieldValidation {
    private readonly name: string;
    private readonly errorMessage: string;
    private readonly args: any;

    constructor(name: string, errorMessage: string, args: any = {}) {
        this.name = name;
        this.errorMessage = errorMessage;
        this.args = args;
    }

    public validate(value: any) {
        let result = false;

        switch (this.name) {
            case 'required':
                result = this.required(value);
                break;
            default:
                result = false;
                break;
        }

        if (result) {
            return new ValidationResult(result);
        }

        return new ValidationResult(result, this.errorMessage);
    }

    public getErrorMessage() {
        return this.errorMessage;
    }

    private required(value: any) {
        return value && value != '';
    }
}