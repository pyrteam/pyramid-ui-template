export class ValidationResult {
    public result: boolean;
    public errorMessage: string;

    constructor(result: boolean, errorMessage: string = '') {
        this.result = result;
        this.errorMessage = errorMessage;
    }
}