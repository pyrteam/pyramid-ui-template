import {FormFieldValidation} from '@app/utils/form/formFieldValidation';

export class FormField {
    private readonly name: string;
    private readonly validations: Array<FormFieldValidation>;
    private defaultValue: any;
    private value: any;
    private _isValid: boolean;
    private _isDirty: boolean;
    private errorMessage: string;

    constructor(name: string, defaultValue: any = undefined, validations: Array<FormFieldValidation> = []) {
        this.name = name;
        this.defaultValue = defaultValue;
        this.value = defaultValue;
        this.validations = validations;

        this.errorMessage = '';
    }

    public updateValue(newValue: any, doNotDirty: boolean) {
        if (!doNotDirty) {
            this.setDirty();
        }

        this.value = newValue;
    }

    public getValue() {
        return this.value;
    }

    public getName() {
        return this.name;
    }

    public validate() {
        this.setDirty();

        let found = this.validations.find((validation: FormFieldValidation) => validation.validate(this.value).result == false);

        if (!found) {
            this.setValid();
            return;
        }

        this.setInvalid(found.getErrorMessage());
    }

    private setValid() {
        this._isValid = true;
        this.errorMessage = '';
    }

    private setInvalid(errorMessage: string) {
        this._isValid = false;
        this.errorMessage = errorMessage;
    }

    public isValid() {
        return this._isValid;
    }

    private setDirty() {
        this._isDirty = true;
    }

    public isDirty() {
        return this._isDirty;
    }

    public stateClass() {
        if (this.validations.length == 0 || !this._isDirty) {
            return '';
        }

        if (this._isValid) {
            return 'valid';
        }

        return 'invalid';
    }

    public toJs() {
        return {
            name: this.name,
            value: this.value,
            isValid: this._isValid,
            stateClass: this.stateClass(),
            errorMessage: this.errorMessage
        };
    }
}