export class List {
    private readonly ID_KEY = 'id';

    private list;
    private ids;

    constructor(list: any = []) {
        this.initialize(list);
    }

    private initialize(list: any) {
        this.list = {};
        this.ids = [];

        if (Array.isArray(list)) {
            list.map(element => {
                this.ids.push(element[this.ID_KEY]);
                this.list[element[this.ID_KEY]] = element;
            });
        }
    }

    public size() {
        return this.ids.length;
    }

    public add(element: any) {
        if (Array.isArray(element)) {
            element.map(el => this.addSingle(el));
            return this;
        }

        this.addSingle(element);

        return this;
    }

    private addSingle(element: any) {
        this.ids = this.ids.concat(element[this.ID_KEY]);
        this.list = Object.assign({}, this.list, {
            [element[this.ID_KEY]]: element
        });
    }

    public get(index: number) {
        if (index < 0) {
            return this.list[this.ids[this.ids.length + index]];
        }

        return this.list[this.ids[index]];
    }

    public find(id: any) {
        return this.list[id];
    }

    public remove(element: any) {
        if (element === undefined) {
            return;
        }

        if (Array.isArray(element)) {
            element.map(el => this.remove(el));
            return this;
        } else if (typeof element === 'number' && isFinite(element)) {
            this.removeOneByIndex(element);
        } else if (typeof element === 'object') {
            this.removeOneById(element[this.ID_KEY]);
        }

        return this;
    }

    private removeOneByIndex(index: number) {
        this.list = Object.assign({}, this.list);
        delete this.list[this.ids[index]];
        this.ids = this.ids.slice(0, index).concat(this.ids.slice(index + 1));
    }

    private removeOneById(id: number) {
        for (let i = 0; i < this.ids.length; i++) {
            if (this.ids[i] == id) {
                this.removeOneByIndex(i);
                return;
            }
        }

        return;
    }

    public toJs() {
        return this.ids.map(id => this.list[id]);
    }
}