export class Entity {
    private readonly ID_KEY = 'id';

    private readonly entity;

    constructor(entity: any = {}) {
        if (Array.isArray(entity) && entity.length > 0) {
            entity = entity[0];
        }
        else if (!entity) {
            entity = {};
        }

        this.entity = entity;
    }

    public toJs() {
        return this.entity;
    }
}
