import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {Home} from '@app/pages/home/home';
import {NotFound} from '@app/pages/notFound/notFound';

const routes: Routes = [
    {
        path: '',
        component: Home
    },
    {
        path: '**',
        component: NotFound
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
