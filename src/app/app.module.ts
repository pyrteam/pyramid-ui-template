import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared';
import { CoreModule } from '@app/core';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {EffectsModule} from '@ngrx/effects';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import {Home} from '@app/pages/home/home';
import {NotFound} from '@app/pages/notFound/notFound';

import {reducers, reducerToken, reducerProvider} from '@app/reducers';
import services from '@app/services';
import effects from '@app/effects';

@NgModule({
    imports: [
        // angular
        BrowserAnimationsModule,
        BrowserModule,

        // core & shared
        CoreModule,
        SharedModule,

        // reducers
        StoreModule.forRoot(reducerToken),
        StoreDevtoolsModule.instrument({
            maxAge: 25
        }),

        // effects
        EffectsModule.forRoot(effects),

        // app
        AppRoutingModule,
    ],
    declarations: [
        AppComponent,
        Home,
        NotFound
    ],
    providers: [
        reducerProvider,
        services,
        Location,
        {provide: LocationStrategy, useClass: PathLocationStrategy}
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {}
