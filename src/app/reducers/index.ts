import {combineReducers, ActionReducerMap} from '@ngrx/store';
import { InjectionToken } from '@angular/core';

export interface AppState {
    app: {
    }
}

export const reducers = combineReducers({
});

export const reducerToken = new InjectionToken<ActionReducerMap<AppState>>('Reducers');

export function getReducers() {
    return {
        app: reducers,
    };
}

export const reducerProvider = [
    { provide: reducerToken, useFactory: getReducers }
];